/*
Aluno: Miguel Reis de Araújo
NºUSP: 12752457
*/

#include <stdio.h>
#include <stdlib.h>

char *read_line();


int main()
{
	int qt_linhas;
	scanf("%d", &qt_linhas);

	scanf("%*c"); // consumindo o \n após a atribuição de qt_linhaas

	char *strings[qt_linhas];

	for (int i = 0; i < qt_linhas; i++) {
		strings[i] = read_line();
	}

	int linhas_validas;
	scanf("%d", &linhas_validas);

	int indices_validos[linhas_validas];

	for (int i = 0; i < linhas_validas; i++) {
		scanf("%d", &indices_validos[i]);
	}

	for (int i = 0; i < linhas_validas; i++) {
		printf("%s\n", strings[indices_validos[i]]);
	}


	//for (int i = 0; i < qt_linhas; i++) {
	//	free(strings[i]);
	//}

	return 0;
}


char *read_line()
{
	int contador_caracteres = 0;
	int indice;
	char *linha_lida;

	do {
		indice = contador_caracteres;

		contador_caracteres += 1;

		if (contador_caracteres == 1) {
			linha_lida = malloc(contador_caracteres * sizeof(char));
		}
		else {
			linha_lida = realloc(linha_lida, contador_caracteres * sizeof(char));
		}

		scanf("%c", &linha_lida[indice]);

	} while (linha_lida[indice] != '\n');

	linha_lida[indice] = '\0';

	return linha_lida;
}
