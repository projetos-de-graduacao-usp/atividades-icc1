#include <stdio.h>

float limite_vel = 80;
float multa;
    
int main()
{
    float veiculo_vel;
    
    scanf("%f", &veiculo_vel);

    if (veiculo_vel > limite_vel) {
	multa = 2 * (veiculo_vel - limite_vel);
	printf("Limite de velocidade excedido! Multado no valor de R$%.2f!\n", multa);	

    } else {
	printf("Velocidade dentro do limite permitido.\n");

    }

    return 0;
}
