#include <stdio.h>
#include <stdlib.h>

int main()
{
	int semente, linhas_a, colunas_b, colunas_a, linhas_b;

	// verificação das dimenções das matrizes e atribuição do valor da seed
	scanf("%i%i", &linhas_a, &colunas_a);

	if (linhas_a <= 0 || colunas_a <= 0) {
		printf("Valores invalidos para a multiplicacao.\n");
		exit(0);
	}

	int matriz_a[linhas_a][colunas_a];

	scanf("%i%i", &linhas_b, &colunas_b);

	if (linhas_b <= 0 || colunas_b <= 0) {
		printf("Valores invalidos para a multiplicacao.\n");
		exit(0);
	}

	int matriz_b[linhas_b][colunas_b];

	if (linhas_b != colunas_a) {
		printf("Valores invalidos para a multiplicacao.\n");
		exit(0);
	}

	int matriz_c[linhas_a][colunas_b];
	// estou zerando a matriz c
	for (int i = 0; i < linhas_a; i++) {
		for (int j = 0; j < colunas_b; j++) {
			matriz_c[i][j] = 0;
		}
	}

	scanf("%d", &semente);
	srand(semente);

	// preenchimento das matrizes a e b
	for (int i = 0; i < linhas_a; i++) {
		for (int j = 0; j < colunas_a; j++) {
			matriz_a[i][j] = rand() % 50 - 25;
		}
	}

	for (int i = 0; i < linhas_b; i++) {
		for (int j = 0; j < colunas_b; j++) {
			matriz_b[i][j] = rand() % 50 - 25;
		}
	}

	// cálculo da matriz C
	for (int i = 0; i < linhas_a; i++) {
		for (int j = 0; j < colunas_b; j++) {
			for (int k = 0; k < colunas_a; k++) {
				matriz_c[i][j] += matriz_a[i][k] * matriz_b[k][j];
			}
		}
	}

	// impressão da matriz C
	for (int i = 0; i < linhas_a; i++) {
		printf("Linha %i:", i);

		for (int j = 0; j < colunas_b; j++) {
			printf(" %i", matriz_c[i][j]);
		}

		printf("\n");
	}
	return 0;
}
