/*
Aluno: Miguel Reis de Araújo
NºUSP: 12752457
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ADICIONAR 1
#define EXIBIR 2
#define PROXIMA 3
#define ANTERIOR 4
#define SALVAR 5
#define CARREGAR 6
#define SAIR 7

typedef struct musica {
	char *nome_musica;
	char *nome_artista;
	int duracao;
} Musica;

typedef struct playlist {
	char *nome_playlist;
	int qt_musicas;
	Musica *lista;
} Playlist;


void adicionar_nova_musica(Playlist *playlist_fornecida);
void exibir_musicas(Playlist *playlist_fornecida, Musica *musica_atual);
char *read_line();
void gravar_playlist(Playlist *playlist_fornecida);
void binaryToNum(char *binFilename);
void carregar_playlist(Playlist *playlist_fornecida);


int main()
{
	Playlist playlist_fornecida;

	playlist_fornecida.nome_playlist = read_line();
	playlist_fornecida.qt_musicas = 0;
	playlist_fornecida.lista = malloc(sizeof(Musica));
	Musica *musica_atual = NULL;
	short unsigned int indice_musica_atual = 0;
	short unsigned int comando;

	do {
		scanf(" %hd", &comando);
		scanf(" %*[\n]");

		switch (comando) {
			case ADICIONAR:
				adicionar_nova_musica(&playlist_fornecida);
				musica_atual = &playlist_fornecida.lista[indice_musica_atual];
				break;

			case EXIBIR: exibir_musicas(&playlist_fornecida, musica_atual); break;

			case PROXIMA:
				indice_musica_atual += 1;
				musica_atual = &playlist_fornecida.lista[indice_musica_atual];
				break;

			case ANTERIOR:
				indice_musica_atual -= 1;
				musica_atual = &playlist_fornecida.lista[indice_musica_atual];
				break;

			case SALVAR: gravar_playlist(&playlist_fornecida); break;

			case CARREGAR:
				carregar_playlist(&playlist_fornecida);
				indice_musica_atual = 0;
				musica_atual = &playlist_fornecida.lista[indice_musica_atual];
				break;

			case SAIR: break;
		}

	} while (comando != SAIR);


	// Fazendo a faxina
	free(playlist_fornecida.nome_playlist);

	for (int i = 0; i < playlist_fornecida.qt_musicas; i++) {
		free(playlist_fornecida.lista[i].nome_musica);
		free(playlist_fornecida.lista[i].nome_artista);
	}

	free(playlist_fornecida.lista);

	return 0;
}


void adicionar_nova_musica(Playlist *playlist)
{
	Musica nova_musica;
	short unsigned int indice = playlist->qt_musicas;

	if (indice == 15) {
		printf("Playlist cheia!\n");
		return;
	}

	nova_musica.nome_musica = read_line();
	nova_musica.nome_artista = read_line();
	scanf("%d", &nova_musica.duracao);
	scanf(" %*[\n]");

	if (indice > 0) {
		playlist->lista = realloc(playlist->lista, (indice + 1) * sizeof(Musica));
	}

	playlist->lista[indice] = nova_musica;

	printf("Musica %s de %s adicionada com sucesso.\n", nova_musica.nome_musica,
		nova_musica.nome_artista);

	playlist->qt_musicas += 1;
}


void exibir_musicas(Playlist *playlist_fornecida, Musica *musica_atual)
{
	Musica *lista = playlist_fornecida->lista;
	short unsigned int qt_musicas = playlist_fornecida->qt_musicas;

	printf("---- Playlist: %s ----\n", playlist_fornecida->nome_playlist);
	printf("Total de musicas: %d\n\n", qt_musicas);

	for (int i = 0; i < qt_musicas; i++) {
		Musica *musica_sendo_mostrada = &lista[i];

		if (musica_sendo_mostrada == musica_atual) {
			printf("=== NOW PLAYING ===\n");
		}

		printf("(%d). \'%s\'\n", i + 1, lista[i].nome_musica);
		printf("Artista: %s\n", lista[i].nome_artista);
		printf("Duracao: %d segundos\n\n", lista[i].duracao);
	}
}


char *read_line()
{
	int contador_caracteres = 0;
	int indice;
	char *linha_lida = malloc(sizeof(char));

	do {
		indice = contador_caracteres;

		contador_caracteres += 1;

		if (contador_caracteres > 1) {
			linha_lida = realloc(linha_lida, contador_caracteres * sizeof(char));
		}

		scanf("%c", &linha_lida[indice]);

	} while (linha_lida[indice] != '\n' && linha_lida[indice] != '\r');

	linha_lida[indice] = '\0';

	scanf(" %*[\n]");

	return linha_lida;
}


void gravar_playlist(Playlist *playlist_fornecida)
{
	char *nome_arquivo = read_line();
	FILE *arquivo = fopen(nome_arquivo, "wb");
	char *nome_playlist = playlist_fornecida->nome_playlist;
	int tamanho_nome_playlist = strlen(nome_playlist);

	fwrite(&tamanho_nome_playlist, sizeof(int), 1, arquivo);
	fwrite(nome_playlist, sizeof(char), tamanho_nome_playlist, arquivo);

	int num_musicas = playlist_fornecida->qt_musicas;
	fwrite(&num_musicas, sizeof(int), 1, arquivo);

	for (int i = 0; i < num_musicas; i++) {
		char *nome_musica = playlist_fornecida->lista[i].nome_musica;
		int tamanho_nome_musica = strlen(nome_musica);

		fwrite(&tamanho_nome_musica, sizeof(int), 1, arquivo);
		fwrite(nome_musica, sizeof(char), tamanho_nome_musica, arquivo);

		char *nome_artista = playlist_fornecida->lista[i].nome_artista;
		int tamanho_nome_artista = strlen(nome_artista);

		fwrite(&tamanho_nome_artista, sizeof(int), 1, arquivo);
		fwrite(nome_artista, sizeof(char), tamanho_nome_artista, arquivo);

		int duracao_musica = playlist_fornecida->lista[i].duracao;

		fwrite(&duracao_musica, sizeof(int), 1, arquivo);
	}

	fclose(arquivo);

	printf("Playlist %s salva com sucesso.\n", nome_arquivo);

	binaryToNum(nome_arquivo);

	free(nome_arquivo);
}


void carregar_playlist(Playlist *playlist)
{
	free(playlist->nome_playlist);

	for (int i = 0; i < playlist->qt_musicas; i++) {
		free(playlist->lista[i].nome_musica);
		free(playlist->lista[i].nome_artista);
	}

	free(playlist->lista);

	char *nome_arquivo = read_line();
	FILE *arquivo = fopen(nome_arquivo, "rb");

	if (arquivo == NULL) {
		printf("Arquivo %s nao existe.\n", nome_arquivo);
		free(nome_arquivo);
		exit(1);
	}

	int tamanho_nome_playlist;
	
	fread(&tamanho_nome_playlist, sizeof(int), 1, arquivo);
	playlist->nome_playlist = malloc((tamanho_nome_playlist + 1) * sizeof(char));
	fread(playlist->nome_playlist, sizeof(char), tamanho_nome_playlist, arquivo);
	playlist->nome_playlist[tamanho_nome_playlist] = '\0';

	fread(&playlist->qt_musicas, sizeof(int), 1, arquivo);

	playlist->lista = malloc(playlist->qt_musicas * sizeof(Musica));

	for (int i = 0; i < playlist->qt_musicas; i++) {
		int tamanho_nome_musica;

		fread(&tamanho_nome_musica, sizeof(int), 1, arquivo);
		playlist->lista[i].nome_musica = malloc((tamanho_nome_musica + 1) * sizeof(char));
		fread(playlist->lista[i].nome_musica, sizeof(char), tamanho_nome_musica, arquivo);
		playlist->lista[i].nome_musica[tamanho_nome_musica] = '\0';


		int tamanho_nome_artista;

		fread(&tamanho_nome_artista, sizeof(int), 1, arquivo);
		playlist->lista[i].nome_artista = malloc((tamanho_nome_artista + 1) * sizeof(char));
		fread(playlist->lista[i].nome_artista, sizeof(char), tamanho_nome_artista, arquivo);
		playlist->lista[i].nome_artista[tamanho_nome_artista] = '\0';

		fread(&playlist->lista[i].duracao, sizeof(int), 1, arquivo);
	}

	fclose(arquivo);

	printf("Playlist %s carregada com sucesso.\n", nome_arquivo);

	binaryToNum(nome_arquivo);

	free(nome_arquivo);
}


void binaryToNum(char *binFilename)
{
	FILE *fp = fopen(binFilename, "rb");

	double binValue = 0;
	double accumulator = 0.01;
	char ch;
	while (fread(&ch, sizeof(char), 1, fp)) {
		binValue += (double)ch * accumulator;
		accumulator += 0.01;
	}

	fclose(fp);
	printf("%lf\n", binValue);
}
