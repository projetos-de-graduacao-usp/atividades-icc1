#include <stdio.h>

int main()
{
    float fahrenheit_temp, celsius_temp;

    scanf("%f", &fahrenheit_temp);

    celsius_temp = (fahrenheit_temp - 32) / 1.8;

    printf("%.2f\n", celsius_temp);
    
    return 0;
}
